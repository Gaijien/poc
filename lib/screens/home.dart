import 'package:flutter/material.dart';
import 'package:flutter_news/screens/home_tabs/Web.dart';
import 'package:flutter_news/screens/home_tabs/favourites.dart';
import 'package:flutter_news/screens/home_tabs/popular.dart';
//import 'package:flutter_news/screens/home_tabs/testreq.dart';
import 'package:flutter_news/screens/home_tabs/whats_new.dart';
import 'package:flutter_news/screens/home_tabs/settings.dart';
import 'package:flutter_news/screens/home_tabs/Web.dart';

class HomeScreen extends StatefulWidget {
  @override
  MyHomeState createState() => MyHomeState();
}

class MyHomeState extends State<HomeScreen> with SingleTickerProviderStateMixin {
  // Create a tab controller
  TabController controller;

  @override
  void initState() {
    super.initState();

    // Initialize the Tab Controller
    controller = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    // Dispose of the Tab Controller
    controller.dispose();
    super.dispose();
  }

  int _selectedIndex = 0;
  static  TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static  List<Widget> _widgetOptions = <Widget>[
    WhatsNew(),
    Popular(), 
    Favourites(),
    SettingsTab(),
    Web()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Appbar
      appBar: AppBar(
        // Title
        title: Text("Just Luxury"),
        // Set the background color of the App Bar
        //backgroundColor: Colors.orangeAccent[100],
        backgroundColor: Colors.black,
      ),
      // Set the TabBar view as the body of the Scaffold
      body: Center(
       child: _widgetOptions.elementAt(_selectedIndex)
      ),
      // Set the bottom navigation bar
      
      bottomNavigationBar: BottomNavigationBar(
        // set the color of the bottom navigation bar
        backgroundColor: Colors.black,
        // set the tab bar as the child of bottom navigation bar
        unselectedItemColor: Colors.grey,
             items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            backgroundColor: Colors.black,
            title: Text('Home'),
          ),
            BottomNavigationBarItem(
            icon: Icon(Icons.watch),
            title: Text('Watches'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.flight),
            title: Text('Travel'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text('Settings'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.https),
            title: Text('Test'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.white,
        onTap: _onItemTapped,
      ),
    );
  }
}