
import 'package:flutter/material.dart';
import 'package:flutter_news/webservice/NewsList.dart';

class Web extends StatefulWidget {
  @override
  _WebState createState() => _WebState();
}

class _WebState extends State<Web> {
  @override
  Widget build(BuildContext context) {
    return NewsList();
  }
}
